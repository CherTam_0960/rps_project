/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.swinghelloproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author kitti
 */
public class Computer {

    private int hand;
    private int playerHand; // r:0, s:1, p:2
    private int win, lose, draw;
    private int status;

    public Computer() {

    }

    public int RPS(int playerHand) { //win:1, draw:0, lose:-1
        System.out.println(playerHand);
        this.playerHand = playerHand;
        random();
        if (this.playerHand == this.hand) {
            draw++;
            status = 0;
            return 0;
        } else if (this.playerHand == 0 && this.hand == 1) {
            win++;
            status = 1;
            return 1;
        } else if (this.playerHand == 1 && this.hand == 2) {
            win++;
            status = 1;
            return 1;
        } else if (this.playerHand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;

    }

    private void random() {
        this.hand = ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
}
